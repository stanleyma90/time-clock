require 'test_helper'

class ClockEventTest < ActiveSupport::TestCase
  
  test "valid clock event" do
    clock_event = ClockEvent.new(name: "Barry Allen", clock_type: "Clock In")
    assert clock_event.valid?
  end
  
  test "clock event invalid without name" do
    clock_event = ClockEvent.new(clock_type: "Clock In")
    refute clock_event.valid?
    assert_not_nil clock_event.errors[:name]
  end
  
  test "clock event invalid without clock type" do
    clock_event = ClockEvent.new(name: "Bruce Wayne")
    refute clock_event.valid?
    assert_not_nil clock_event.errors[:clock_type]
  end
  
end
