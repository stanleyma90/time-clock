require 'test_helper'

class ClockEventsControllerTest < ActionDispatch::IntegrationTest
  
  test "should get index page of clock events" do
    get clock_events_path
    assert_response :success
  end
  
  test "should get edit page of clock event" do
    clock_event = clock_events(:one)
    get edit_clock_event_path(clock_event)
    assert_response :success
  end
  
  test "should get new page of clock event" do
    get new_clock_event_path
    assert_response :success
  end
  
  test "should create a clock event" do
    clock_event_params = {
      name: "Oliver Queen",
      clock_type: "Clock Out",
    }
    
    post(
      clock_events_path,
      params: clock_event_params
    )
    
    assert_redirected_to clock_events_path
    assert_equal "Successfully created", flash[:success] 
  end
  
  test "should update a clock event" do
    clock_event = clock_events(:one)
    
    clock_event_params = {
      name: "Jay",
      clock_type: "Clock Out"
    }
      
    clock_event.update(clock_event_params)
    
    assert clock_event["name"] == clock_event_params[:name]
    assert clock_event["clock_type"] == clock_event_params[:clock_type]
  end
  
  test "should destroy a clock event" do
    clock_event = clock_events(:one)
    clock_event.destroy
      
    assert clock_event.destroyed?
  end
  
end
