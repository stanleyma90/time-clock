module ApplicationHelper
  def bulma_class_for(flash_type)
    case flash_type
    when "success"
      "is-success"
    when "alert"
      "is-warning"
    else
      "is-primary"
    end
  end
end
