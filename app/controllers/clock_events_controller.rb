class ClockEventsController < ApplicationController
  def index
    @clock_events = ClockEvent.paginate(:page => params[:page], :per_page => 20)
  end
  
  def new
    @clock_event = ClockEvent.new
  end
  
  def create
    @clock_event = ClockEvent.new(clock_events_params)
    
    if @clock_event.save
      redirect_to clock_events_path
      flash[:success] = "Successfully created"
    else
      flash[:alert] = "Please enter a name"
      render :new
    end
  end
  
  def edit
    @clock_event = ClockEvent.find(params[:id])
  end
  
  def update
    @clock_event = ClockEvent.find(params[:id])
    
    if @clock_event.update(clock_events_params)
      redirect_to clock_events_path
      flash[:success] = "Successfully updated"
    else
      flash[:alert] = "Update failed"
      render :edit
    end
  end
  
  def destroy
    @clock_event = ClockEvent.find(params[:id])
    
    if @clock_event.destroy
      redirect_to clock_events_path
      flash[:success] = "Successfully deleted"
    else
      flash[:alert] = "Delete failed"
    end
  end
  
  private
  
  def clock_events_params
    params.permit(:name, :clock_type)
  end
end
