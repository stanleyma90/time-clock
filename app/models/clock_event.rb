class ClockEvent < ApplicationRecord
  validates :name, presence: true
  validates :clock_type, presence: true
end
