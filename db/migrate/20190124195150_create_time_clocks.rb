class CreateTimeClocks < ActiveRecord::Migration[5.2]
  def change
    create_table :time_clocks do |t|
      t.string :category

      t.timestamps
    end
  end
end
