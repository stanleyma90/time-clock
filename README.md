## Setup

First, install gems:
```
bundle install
```

Then, setup database:
```
rails db:create
rails db:migrate
rails db:seed
```